
![Screenshot](/uploads/aaebd178a02fd00ac5ef048ec0e7a13c/display.png)

# signup-form
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
